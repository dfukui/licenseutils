/*  Copyright (C) 2013 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "tutorial.h"
#include "warranty.h"
#include "help.h"
#include "gettext-more.h"

#undef TUTORIAL_DOC
#define TUTORIAL_DOC N_("Show a tutorial on how to add a license to a file.")
static struct argp argp = { NULL, NULL, "", TUTORIAL_DOC};

int 
lu_tutorial_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_tutorial_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_tutorial (state, &opts);
  else
    return err;
}

int 
lu_tutorial (struct lu_state_t *state, struct lu_tutorial_options_t *options)
{
  fprintf (state->out, "\
  licensing> echo << EOF > /tmp/foo\n\
> #!/bin/bash\n\
> echo hello world\n\
> EOF\n\
\n\
licensing> boilerplate /tmp/foo\n\
boilerplate: no boilerplate found in /tmp/foo\n\
\n\
licensing> copyright ben 2017\n\
Copyright (C) 2017 ben\n\
copyright: Added.\n\
\n\
licensing> choose gpl shell\n\
choose: Selected.\n\
\n\
# gplv3+> apply /tmp/foo\n\
apply: /tmp/foo -> Boilerplate applied.\n\
\n\
# gplv3+> boilerplate /tmp/foo\n\
# Copyright (C) 2017 ben\n\
#\n\
# This program is free software: you can redistribute it and/or modify\n\
# it under the terms of the GNU General Public License as published by\n\
# the Free Software Foundation, either version 3 of the License, or\n\
# (at your option) any later version.\n\
#\n\
# This program is distributed in the hope that it will be useful,\n\
# but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
# GNU General Public License for more details.\n\
#\n\
# You should have received a copy of the GNU General Public License\n\
# along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\
\n\
# gplv3+> new-boilerplate\n\
licensing>\n");
  return 0;
}

struct lu_command_t tutorial = 
{
  .name         = N_("tutorial"),
  .doc          = TUTORIAL_DOC,
  .flags        = SHOW_IN_HELP | DO_NOT_SAVE_IN_HISTORY,
  .argp         = &argp,
  .parser       = lu_tutorial_parse_argp
};
