/*  Copyright (C) 2013, 2014 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "fdl.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

enum lgpl_version_opts
{
  OPT_V11 = -411,
  OPT_V12,
  OPT_V13,
};

static struct argp_option argp_options[] = 
{
    {"full", 'f', NULL, 0, N_("show the full license text")},
    {"v1.1", OPT_V11, NULL, 0, N_("show version 1.1 of the fdl")},
    {"v1.2", OPT_V12, NULL, 0, N_("show version 1.2 of the fdl")},
    {"v1.3", OPT_V13, NULL, 0, N_("show version 1.3 of the fdl")},
    {"jerkwad", 'j', NULL, 0, N_("remove the or-any-later-version clause")},
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_fdl_options_t *opt = NULL;
  if (state)
    opt = (struct lu_fdl_options_t*) state->input;
  switch (key)
    {
    case 'l':
        {
          int i = 0;
          while (fdl.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", fdl.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case 'j':
      opt->future_versions = 0;
      break;
    case 'f':
      opt->full = 1;
      break;
    case OPT_V11:
      opt->version = 1;
      break;
    case OPT_V12:
      opt->version = 2;
      break;
    case OPT_V13:
      opt->version = 3;
      break;
    case ARGP_KEY_INIT:
      opt->full = 0;
      opt->version = 3;
      opt->future_versions = 1;
      break;
    case ARGP_KEY_FINI:
      if (opt->future_versions == 0 && opt->full)
        {
          argp_failure (state, 0, 0, 
                        N_("--jerkwad cannot be used with --full"));
          argp_state_help (state, stderr, ARGP_HELP_STD_ERR);
        }
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef FDL_DOC
#define FDL_DOC N_("Show the GNU Free Documentation License notice.")
static struct argp argp = { argp_options, parse_opt, "", FDL_DOC};

int 
lu_fdl_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_fdl_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_fdl (state, &opts);
  else
    return err;
}

int
show_lu_fdl(struct lu_state_t *state, struct lu_fdl_options_t *options)
{
  char *file;
  if (options->version == 1)
    file = strdup ("old-licenses/fdl-1.1");
  else if (options->version == 2)
    file = strdup ("old-licenses/fdl-1.2");
  else
    file = strdup ("fdl-1.3");
  char *url = xasprintf ("%s/licenses/%s%s.%s", GNU_SITE, file,
                         "", "txt");
  char *data = NULL;
  int err = download (state, url, &data);
  free (url);
  if (options->full)
    luprintf (state, "%s\n", data);
  else
    {
      int replace = !options->future_versions;
      switch (options->version)
        {
        case 1:
          err = show_lines_after 
            (state, data, "      Permission is granted to copy,", 7, replace,
             "or any later version", "of the License as");
          break;
        case 2:
          err = show_lines_after (state, data, 
                                  "    Permission is granted to copy,", 
                                  6, replace, "or any later version", 
                                  "of the License as");
          break;
        case 3:
          err = show_lines_after (state, data, 
                                  "    Permission is granted to copy,", 
                                  6, replace, "or any later version", 
                                  "of the License as");
          break;
        }
    }
  free (data);
  return err;
}

int 
lu_fdl (struct lu_state_t *state, struct lu_fdl_options_t *options)
{
  int err = 0;
  err = show_lu_fdl(state, options);
  return err;
}
struct lu_command_t fdl = 
{
  .name         = N_("fdl"),
  .doc          = FDL_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_fdl_parse_argp,
  .latest_idx   = 3,
  .mentions     =
    {
      " the FDL ",
      " Free Documentation License ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "fdlv1.1,full-license",
          .cmd       = "fdl --v1.1 --full",
          .words     = "GNU Free,Version 1.1",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.2,full-license",
          .cmd       = "fdl --v1.2 --full",
          .words     = "GNU Free,Version 1.2",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.3,full-license",
          .cmd       = "fdl --v1.3 --full",
          .words     = "GNU Free,Version 1.3",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.3+",
          .cmd       = "fdl --v1.3",
          .words     = "GNU Free,Version 1.3",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.3,no-later",
          .cmd       = "fdl --v1.3 --jerkwad",
          .words     = "GNU Free,Version 1.3",
          .recommend = 0
        },
        {
          .keyword   = "fdlv1.2+",
          .cmd       = "fdl --v1.2",
          .words     = "GNU Free,Version 1.2",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.2,no-later",
          .cmd       = "fdl --v1.2 --jerkwad",
          .words     = "GNU Free,Version 1.2",
          .recommend = 0
        },
        {
          .keyword   = "fdlv1.1+",
          .cmd       = "fdl --v1.1",
          .words     = "fdlv11+ GNU Free,Version 1.1",
          .recommend = 1
        },
        {
          .keyword   = "fdlv1.1,no-later",
          .cmd       = "fdl --v1.1 --jerkwad",
          .words     = "GNU Free,Version 1.1",
          .recommend = 0
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};
